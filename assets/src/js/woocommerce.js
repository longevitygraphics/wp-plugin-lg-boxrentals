(function($) {

        $(document).ready(function() {

	        var lgBoxRentals = {
	            pd: "",
	            price: "",
	            duration: "",
	            boxProductID: "",
	            spinnerHTML: "",
	            tempProduct: $("#tempProduct").attr('data-product-id'),
	            addToCart: function(product_id) {
	                var data = {
	                    action: 'woocommerce_ajax_add_to_cart',
	                    product_id: product_id,
	                    quantity: 1
	                };

	                $.post('/wp-admin/admin-ajax.php', data, function(response) {
	                    if (response != 0) {
	                        lgBoxRentals.refreshCartCheckout();
	                    } else {
	                        console.log("Error Adding to Cart");
	                    }
	                });
	            },
	            removeFromCart: function(product_id) {
	                var data = {
	                    action: 'remove_item_from_cart',
	                    product_id: product_id,
	                    quantity: 1
	                }

	                $.ajax({
	                    type: "POST",
	                    url: '/wp-admin/admin-ajax.php',
	                    data: data,
	                    success: function(res) {
	                        if (res) {
	                            lgBoxRentals.refreshCartCheckout();
	                            //console.log(product_id + " Removed from Cart");
	                        }
	                    },
	                    error: function(MLHttpRequest, textStatus, errorThrown) { alert(errorThrown); }

	                });
	            },
	            moveProgressIndicator: function() {
	                var tracker = $('.progress-slider')
	                var trackerOffset = $('.progress-tracker').offset()
	                var trackerPosition = tracker.find(".active").offset();
	                trackerPosition = trackerPosition["left"] + (tracker.find(".active").outerWidth() / 2) - 12.5 - trackerOffset["left"];
	                console.log(trackerPosition);
	                $(".progress-icon").transition({ x: trackerPosition }, 500, 'ease');
	            },
	            lgUpdatePrice: function() {
	                lgBoxRentals.duration = $('.duration-slide.selected').attr('data-duration');
	                var basePrice = $('.product-slide.selected').attr('data-product-base-price');
	                var blockPrice = $('.product-slide.selected').attr('data-product-block-price');
	                lgBoxRentals.boxProductID = $('.product-slide.selected').attr('data-product-id');

	                if (!isNaN(basePrice) && !isNaN(blockPrice) && !isNaN(lgBoxRentals.duration)) {

	                    lgBoxRentals.price = (parseInt(lgBoxRentals.duration) * parseInt(blockPrice)) + parseInt(basePrice);
	                    $("#show-price").text(lgBoxRentals.price);
	                    var summary = $('.product-slide.selected').find(".product-description").html()
	                    var durChosen = $(".duration-slide.selected").text();
	                    var boxChosen = $(".product-slide.selected").find(".product-name").html();
	                    //console.log("Summary: " + summary);
	                    $("#show-product").html(boxChosen);
	                    $("#show-duration").html(durChosen);
	                    $(".product-summary").html(summary);

	                    var md = parseInt($("#delivery_date").datepicker("option", "minDate"));
	                    var pd = md + (7 * parseInt(lgBoxRentals.duration));
	                    var pr = pd + 2;
	                    pd = "+" + pd;
	                    pr = "+" + pr;

	                    $("#pickup_date").datepicker("option", "minDate", pd);
	                    $("#pickup_date").datepicker("option", "maxDate", pr);

	                } else {
	                    console.log("Error");
	                }
	            },
	            fixSlick: function() {
	                $(".slick").slick('refresh');
	            },
	            refreshCartCheckout: function() {
	                $.ajax({
	                    type: 'GET',
	                    url: '/wp-admin/admin-ajax.php',
	                    data: { action: 'replaceCart' },
	                    success: function(textStatus) {
	                        $('.addons-price-summary').html(textStatus);
	                        //console.log("Cart Refreshed!");
	                    },
	                    error: function(MLHttpRequest, textStatus, errorThrown) {
	                        alert(errorThrown);
	                    }
	                });
	                $.ajax({
	                    type: 'GET',
	                    url: '/wp-admin/admin-ajax.php',
	                    data: { action: 'replaceCheckout' },
	                    success: function(textStatus) {
	                        $('.woocommerce-checkout-review-order-table').html(textStatus);
	                        //console.log("Checkout Refreshed!");
	                    },
	                    error: function(MLHttpRequest, textStatus, errorThrown) {
	                        alert(errorThrown);
	                    }
	                });
	            },
	            box_product_slider: function() {
	                var bType = $("#res-com-container").find(".selected").attr("data-value");
	                var pType = $("#box-package-container").find(".selected").attr("data-value");

	                if (pType == "packages") {
	                    $(".boxes-header").text("Select your rental duration and your package");
	                    $("#show-product").empty();
	                    //$("#show-duration").empty();
	                    $("#show-price").empty();
	                    $(".product-summary").empty();
	                } else {
	                    $(".boxes-header").text("Select your rental duration and the quantity of boxes");
	                }

	                var data = {
	                    action: 'box_product_slider',
	                    businessType: bType,
	                    packageType: pType
	                };

	                $.post('/wp-admin/admin-ajax.php', data, function(response) {
	                    if (response != 0) {
	                        lgBoxRentals.spinnerHTML = $('.boxes-product-slider').html();
	                        $('.boxes-product-slider').slideUp();
	                        $('.boxes-product-slider').slick('refresh').slick('removeSlide', null, null, true);
	                        $('.boxes-product-slider').empty().html(response);
	                        $('.boxes-product-slider').slick('destroy').slick('refresh').slideDown();
	                    } else {
	                        console.log("Failed Ajax Request");
	                    }
	                });
	            },
	            validateCurrent: function(current) {
	                var allInputs = current.find(".validate-required");
	                var passValidation = true;

	                for (var x = 0; x < allInputs.length; x++) {
	                    var removeWarning = true;
	                    var match;
	                    if ($(allInputs[x]).has("input, textarea, select").length) {
	                        if (!$(allInputs[x]).find("input, textarea, select").prop("value")) {
	                            console.log($(allInputs[x]).prop('id') + ": Blank Required Field: " + $(allInputs[x]).prop('value'));
	                            $(allInputs[x]).addClass("woocommerce-invalid-required-field");
	                            passValidation = false;
	                            removeWarning = false;
	                            //console.log("passValidation: " + passValidation);
	                        }
	                    }
	                    if ($(allInputs[x]).has("select").length) {
	                        if ($(allInputs[x]).find("select").prop("value") == "select" || !$(allInputs[x]).find("select").prop("value")) {
	                            console.log($(allInputs[x]).prop('id') + ": Select Default Field: " + $(allInputs[x]).prop('value'));
	                            $(allInputs[x]).addClass("woocommerce-invalid-required-field");
	                            passValidation = false;
	                            removeWarning = false;
	                            //console.log("passValidation: " + passValidation);
	                        }
	                    }
	                    if ($(allInputs[x]).find("input").prop("autocomplete") == "postal-code") {
	                        var regex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
	                        var inputLength = $(allInputs[x]).find("input").prop("value").length;
	                        var value = $(allInputs[x]).find("input").prop("value");
	                        match = regex.exec(value);
	                        if (match) {
	                            if ((value.indexOf("-") !== -1 || value.indexOf(" ") !== -1) && inputLength == 7) {
	                                //console.log("Valid Postal Code - No Dash");

	                            } else if ((value.indexOf("-") == -1 || value.indexOf(" ") == -1) && inputLength == 6) {
	                                //console.log("Valid Postal Code - With Dash");
	                            }
	                        } else {
	                            passValidation = false;
	                            removeWarning = false;
	                            $(allInputs[x]).addClass("woocommerce-invalid-required-field");
	                            if (inputLength != 0 && inputLength) {
	                                console.log("Postal Code is Incorrectly Formatted");
	                                $(allInputs[x]).children(".error").html("Postcode format should be XXX XXX");
	                            } else {
	                                console.log("Postal Code is Missing");
	                                $(allInputs[x]).children(".error").html("Postcode is Required");
	                            }
	                        }
	                    }

	                    if ($(allInputs[x]).find(".slide").length) {
	                        if (!$(allInputs[x]).find(".selected").length) {
	                            passValidation = false;
	                            removeWarning = false;
	                            console.log($(allInputs[x]).prop('class') + ": Missing Required Select");
	                            $(allInputs[x]).addClass("woocommerce-invalid-required-field");
	                        }
	                    }


	                    if (removeWarning == true) {
	                        $(allInputs[x]).removeClass("woocommerce-invalid-required-field");
	                    }
	                }
	                return passValidation;
	            },
	            updateQuantity: function() {
	                var quantity = $(this).prop("value");
	                var cartItemKey = $(this).prop("id");
	                var maxItems = $(this).prop('max');

	                if (!maxItems) {
	                    maxItems = "none";
	                }

	                var data = {
	                    action: 'lg_update_cart_quantity',
	                    quantity: quantity,
	                    cart_item: cartItemKey,
	                    max_quantity: maxItems,
	                };

	                $.post('/wp-admin/admin-ajax.php', data, function(response) {
	                    if (response != 0) {
	                        console.log("Ajax Success");
	                        refreshCartCheckout();
	                    }
	                });
	            },
	            readd_spinners: function() {
	                $('.boxes-product-slider').empty().html(spinnerHTML);
	            },
	            productSlideClickHandler: function(event) {
	                var selSlid = $(event.target).closest(".product-slide");
	                selSlid.addClass("selected").siblings().removeClass("selected");
	                var pName = selSlid.find(".product-name").html();
	                $("#show-product").html(pName);
	                lgBoxRentals.lgUpdatePrice();
	            },
	            durationSlideClickHandler: function() {
	                $(this).addClass("selected").siblings().removeClass("selected");
	                var pName = $(this).text();
	                $("#show-duration").html(pName);
	                lgBoxRentals.lgUpdatePrice();
	            },
	            addonAddClickHandler: function() {
	                var x = $(this).closest(".addon-slide").prop('id');
	                console.log(x);
	                lgBoxRentals.addToCart(x);
	            },
	            removeClickHandler: function() {
	                console.log(this);
	                var x = $(this).attr('data-product_id');
	                console.log(x);
	                lgBoxRentals.removeFromCart(x);
	            },
	            nextClickHandler: function() {
	                // Variables
	                var current = $(this).parent().parent(); // Current Section Object
	                var next = current.next(); // Next Section Object
	                var validates = lgBoxRentals.validateCurrent(current); // Whether Fields in current section validate
	                var currentID = "#ps-" + current.prop('id'); // Gets ID of current progress Slider Item
	                var nextID = "#ps-" + next.prop('id'); // Gets ID of next progress Slider Item (i.e the item we are switching to)
	                var moved;

	                if (validates == true) {
	                    // Manipulating Section Object
	                    current.removeClass("show").removeClass("d-flex").addClass("hide"); // Removes Show and D-flex from current section, adds hide
	                    next.removeClass("hide").addClass("show").addClass("d-flex"); //Adds Show and D-flex to next section, removes hide
	                    lgBoxRentals.fixSlick(); // Runs program that reinits various sliders due to them going from d-none to d-flex

	                    //Manipulating Pill Object  
	                    if (currentID != "#ps-residential-commercial") {
	                        $(".progress-slider").slick('slickNext'); // Moves Progress Slider Along 1
	                        if ($(currentID).hasClass("slick-slide")) {
	                            moved = true;
	                        }
	                    }

	                    $(nextID).siblings().removeClass('active'); // Removes Active from all other Slider Items
	                    $(nextID).prevAll().addClass("done"); // Adds Class Done to all previous slider items
	                    $(nextID).addClass('active'); // Adds Class active to Next Item 

	                    if (moved != true) {
	                        lgBoxRentals.moveProgressIndicator();
	                    }
	                    if ($(currentID).hasClass("slick-slide")) {
	                        $(window).scrollTop(0);
	                    }
	                }
	            },
	            backClickHandler: function() {
	                // Variables
	                var current = $(this).parent().parent(); // Section Object
	                var next = current.prev(); // Previous Section Object
	                var currentID = "#ps-" + current.prop('id'); // Nav Pill Object
	                var nextID = "#ps-" + next.prop('id'); // Previous Nav Pill Object

	                // Manipulating Section Object
	                current.removeClass("show").removeClass("d-flex").addClass("hide"); //Hide Current Section, Stop Hiding Previous Section
	                next.removeClass("hide").addClass("show").addClass("d-flex"); //Show Previous Section, Stop Showing Current Section
	                lgBoxRentals.fixSlick(); // Reinitialize Sliders that were initialized while hidden


	                $(".progress-slider").slick('slickPrev'); // Trigger Previous Slide for Slick Slider

	                $(currentID).removeClass('active'); // Remove active styling from Pill Object
	                $(nextID).removeClass("done").nextAll().removeClass("done"); // Remove Done styling from Previous Pill Object and all Further Pill Objects
	                $(nextID).addClass('active'); // Add Active styling to previous pill Object.

	                lgBoxRentals.moveProgressIndicator();
	            },
	            progressSlideClickHandler: function() {
	            	console.log(this);
	                var switchTo = this.id;
	                $(this).addClass("active").siblings().removeClass("active");
	                switchTo = "#" + switchTo.substring(3, switchTo.length);
	                console.log(switchTo);
	                $(switchTo).addClass("show d-flex").removeClass("hide").siblings().removeClass("show").removeClass("d-flex").addClass("hide");
	                $(this).prevAll().addClass("done");
	                $(this).removeClass("done").nextAll().removeClass("done");
	                lgBoxRentals.fixSlick();

	                var nextPos = $(this).position();
	                console.log(nextPos["left"]);
	                $(".progress-icon").transition({ x: nextPos["left"] }, 500, 'ease');
	            },
	            nextFromResCommClickHandler: function() {
	                lgBoxRentals.removeFromCart(lgBoxRentals.tempProduct);
	            },
	            nextFromPickupClickHandler: function() {
	                if (boxProductID && price) {
	                    var data = {
	                        action: 'lg_calculate_price',
	                        product_id: boxProductID,
	                        price: price,
	                    }
	                    $.post('/wp-admin/admin-ajax.php', data, function(response) {
	                        if (response != 0) {
	                            //console.log("Ajax Success");
	                            refreshCartCheckout();
	                        } else {
	                            console.log("Ajax Failure");
	                        }
	                        //console.log(response);
	                    });
	                } else {
	                    alert("Invalid Product");
	                }
	                lgBoxRentals.refreshCartCheckout();
	            },
	            backFromBillingClickHandler: function() {
	                console.log("Back-from-billing click info");
	                lgBoxRentals.removeFromCart(boxProductID);
	            },
	            nextFromPackageClickHandler: function() {
	                lgBoxRentals.box_product_slider();
	            },
	            backFromBoxesClickHandler: function() {
	                lgBoxRentals.readd_spinners();
	            },
	            twoPanelClickHandler() {
	                $(this).addClass("selected").siblings().removeClass("selected");
	                var x = $(".res-com-container").find(".selected").attr("data-value");
	            },
	            updateQuantityHandler() {
	                lgBoxRentals.updateQuantity(this);
	            }
	        };

	        lgBoxRentals.addToCart(lgBoxRentals.tempProduct);
	        lgBoxRentals.moveProgressIndicator();

	        $('.progress-slider').on('afterChange', function(event, slick, currentSlide, nextSlide) {
	            lgBoxRentals.moveProgressIndicator();
	        });

	        $('.boxes-duration-slider').on('afterChange', function(event, slick, currentSlide, nextSlide) {
	            var newSelected = $('.boxes-duration-slider').find(`[data-slick-index='${currentSlide}']`);
	            newSelected.addClass("selected").siblings().removeClass("selected");
	            lgBoxRentals.lgUpdatePrice();
	        });

	        $('.boxes-product-slider').on('afterChange', function(event, slick, currentSlide, nextSlide) {
	            var newSelected = $('.boxes-product-slider').find(`[data-slick-index='${currentSlide}']`);
	            newSelected.addClass("selected").siblings().removeClass("selected");
	            lgBoxRentals.lgUpdatePrice();
	        });

	        $('.boxes-product-slider').slick({
	            arrows: true,
	            autoplay: false,
	            slidesToShow: 5,
	            slidesToScroll: 1,
	            responsive: [{
	                    breakpoint: 768,
	                    settings: {
	                        slidesToShow: 4,
	                    }
	                },
	                {
	                    breakpoint: 767,
	                    settings: {
	                        slidesToShow: 1
	                    }
	                }
	            ]
	        }).slick('destroy');


	        $('.boxes-duration-slider').slick({
	            arrows: false,
	            autoplay: false,
	            slidesToShow: 4,
	            slidesToScroll: 1,
	            responsive: [{
	                breakpoint: 768,
	                settings: {
	                    slidesToShow: 1,
	                    slidesToScroll: 1,
	                    arrows: true,
	                }
	            }]
	        });

	        $('.addon-slider').slick({
	            arrows: true,
	            autoplay: false,
	            slidesToShow: 6,
	            slidesToScroll: 1,
	            infinite: true,
	            responsive: [{
	                    breakpoint: 768,
	                    settings: {
	                        arrows: true,
	                        slidesToShow: 4,
	                    }
	                },
	                {
	                    breakpoint: 525,
	                    settings: {
	                        arrows: true,
	                        slidesToShow: 3,
	                    }
	                },
	                {
	                    breakpoint: 415,
	                    settings: {
	                        arrows: true,
	                        slidesToShow: 2,
	                    }
	                }
	            ]
	        });


	        $('.progress-slider').slick({
	            arrows: false,
	            infinite: false,
	            responsive: [{
	                    breakpoint: 9999,
	                    settings: "unslick",
	                },
	                {
	                    breakpoint: 975,
	                    settings: {
	                        slidesToShow: 3,
	                        slidesToScroll: 1,
	                    },
	                },
	                {
	                    breakpoint: 768,
	                    settings: {
	                        slidesToShow: 3,
	                        slidesToScroll: 1,
	                    },
	                }
	            ]
	        });

	        $("#delivery_date").datepicker({ // Initializes and locks Delivery Date
	            minDate: "+2",
	            maxDate: "+3M",
	            dateFormat: "yy-mm-dd",
	            beforeShowDay: function(date) {
	                var day = date.getDay();
	                return [(day != 0)];
	            }
	        });

	        $("#pickup_date").datepicker({ // Initializes Pickup Date, date is locked in lgUpdatePrice()
	            minDate: "+9",
	            maxDate: "+11",
	            dateFormat: "yy-mm-dd",
	            beforeShowDay: function(date) {
	                var day = date.getDay();
	                return [(day != 0)];
	            }
	        });

	        $(document).on('click', '.product-slide', lgBoxRentals.productSlideClickHandler);

	        $(".duration-slide").click(lgBoxRentals.durationSlideClickHandler);

	        $(".addon.add").click(lgBoxRentals.addonAddClickHandler);

	        $(document).on('click', '.remove', lgBoxRentals.removeClickHandler);

	        $(".next").click(lgBoxRentals.nextClickHandler);

	        $(".back").click(lgBoxRentals.backClickHandler);

	        //$('.progress-slide').click(lgBoxRentals.progressSlideClickHandler(this));

	        $("#next-from-residential-commercial").click(lgBoxRentals.nextFromResCommClickHandler);

	        $("#next-from-pickup").click(lgBoxRentals.nextFromPickupClickHandler);

	        $("#back-from-billing").click(lgBoxRentals.backFromBillingClickHandler);

	        $("#next-from-package-type").click(lgBoxRentals.nextFromPackageClickHandler);

	        $("#back-from-boxes").click(lgBoxRentals.backFromBoxesClickHandler);

	        $(".two-panel").click(lgBoxRentals.twoPanelClickHandler);

	        $(document).on('click change focusout', 'input.qty', lgBoxRentals.updateQuantityHandler);

	        $("select#delivery_state, select#pickup_state").prop("value", "BC");

	        $("select").selectric({ arrowButtonMarkup: '<b class="button"><i class="fas fa-chevron-down"></i></b>' });
    });

}(jQuery));