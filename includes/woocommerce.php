<?php 
// Add Box Rental Product Type
	if ( ! function_exists('lg_write_log')) {
	     function lg_write_log ( $log )  {
	        if ( is_array( $log ) || is_object( $log ) ) {
	           error_log( print_r( $log, true ) );
	        } else {
	           error_log( $log );
	        }
	     }
	  }

	add_action( 'init', 'lg_create_custom_product_type');

	// Adds Custom Product Type
	function lg_create_custom_product_type(){
		class WC_Product_Box_Rental extends WC_Product {
			public function __construct( $product ) {
        		$this->product_type = 'box_rental';
    			parent::__construct( $product );
    		}
		}
	}

	add_filter ( 'product_type_selector', 'lg_add_custom_product_type');

	// Adds Adds Selector to choose Box Rental Product type in WooComm Backend
	function lg_add_custom_product_type ($types) {
		$types['box_rental'] = __( 'Box Rental Product', 'box_rental' );
		return $types;
	}

	add_filter( 'woocommerce_product_data_tabs', 'rental_product_tab' );

	// Adds Pricing Tab for Box Rental type in WooComm Backend
	function rental_product_tab ( $tabs ) {
		$tabs['box_rental'] = array(
			'label'		=>	__( 'Pricing', 'box_rental' ),
			'target'	=> 'rental_product_options',
			'class' 	=> 'show_if_box_rental',
			'priority'	=> 1, 
		);
		//$tabs['inventory']['class'][] = 'show_if_box_rental';

		$tabs['attribute']['class'][] = 'hide_if_box_rental';
        $tabs['shipping']['class'][] = 'hide_if_box_rental';
        $tabs['inventory']['class'][] = 'hide_if_box_rental';
        $tabs['linked_product']['class'][] = 'hide_if_box_rental';

		return $tabs;
	}

	add_action	('woocommerce_box_rental_add_to_cart', 'box_rental_add_to_cart_link' );
	function box_rental_add_to_cart_link() {
		wc_get_template( 'single-product/add-to-cart/simple.php' );
	}


	add_action('admin_footer', 'box_rental_custom_js');
	function box_rental_custom_js() {
		if ('product' != get_post_type()) :
			return;
		endif; ?>	
		<script type='text/javascript'>
            jQuery( '.options_group.pricing' ).addClass( 'show_if_box_rental' );
            jQuery( '.inventory_product_data' ).addClass( 'show_if_box_rental' );
        </script> <?php
	}
		
	add_action( 'woocommerce_product_data_panels', 'rental_product_tab_product_tab_content');

	// Adds Contnet to Pricing Tab in Woo Commerce Backend
	function rental_product_tab_product_tab_content() {
		?><div id='rental_product_options' class ='panel woocommerce_options_panel'><?php
		?><div class='options_group'><?php

		woocommerce_wp_text_input(
			array(
				'id' => "rental_base_price",
				'label' => __( 'Box Rental Base Price ($)', 'box_rental' ),
				'placeholder' => '',
				'desc_tip' => 'true',
				'description' => __( 'Enter Box Base Price.', 'box_rental' ),
				'type' => 'number',
				'class' => 'wc_input_price short',
				'step' => '.01'
			));

		woocommerce_wp_text_input(
			array(
				'id' => "rental_block_price",
				'label' => __( 'Box Rental Base Price ($)', 'box_rental' ),
				'placeholder' => '',
				'desc_tip' => 'true',
				'description' => __( 'Enter Box Weekly Price.', 'box_rental' ),
				'type' => 'number',
				'class' => 'wc_input_price short',
				'step' => '.01'
			));
		?></div></div><?php
	}


	// Saves Custom Product Options to Database.
	add_action( 'woocommerce_process_product_meta', 'save_rental_product_settings', 10, 1);
	function save_rental_product_settings ($post_id) {
		
		$rental_block_price = $_POST['rental_block_price'];
		$rental_base_price = $_POST['rental_base_price'];

		if(isset($_POST['rental_block_price'])) {
			update_post_meta( $post_id, 'rental_block_price', esc_attr( $rental_block_price));
		}

		if(isset($_POST['rental_base_price'])) {
			update_post_meta( $post_id, 'rental_base_price', esc_attr( $rental_base_price));
		}
	}


	// Disables Coupons
	add_filter( 'woocommerce_coupons_enabled', 'disable_coupons_field' );
	function disable_coupons_field( $enabled) {
    	$enabled = false;
    	return $enabled;
    }

 
    // Disables Empy Cart Message
    add_action('woocommerce_cart_is_empty', 'empty_cart_page');
    function empty_cart_page() {
    	return '';
    }

    // Disables Empy Cart Custom Message
	add_filter( 'wc_empty_cart_message', 'custom_wc_empty_cart_message' );
	function custom_wc_empty_cart_message() {
  		return '';
	}

	// Adds Action for pickup form
	add_action ('woocommerce_pickup_details', 'checkout_form_pickup');
	function checkout_form_pickup() {
		wc_get_template("checkout/form-pickup.php");
	};

	// Adds Action for pickup form
	add_action ('woocommerce_delivery_details', 'checkout_form_delivery');
	function checkout_form_delivery() {
		wc_get_template("checkout/form-delivery.php");
	};

	// Adds Custom Fields to Woocommerce Checkout
	add_filter('woocommerce_checkout_fields', 'custom_shipping_fields');
	function custom_shipping_fields ($fields) {

/*		unset($fields['shipping']['shipping_first_name']);
		unset($fields['shipping']['shipping_last_name']);
		unset($fields['shipping']['shipping_company']);
		unset($fields['shipping']['shipping_address_1']);
		unset($fields['shipping']['shipping_address_2']);
		unset($fields['shipping']['shipping_city']);
		unset($fields['shipping']['shipping_country']);
		unset($fields['shipping']['shipping_postcode']);
		unset($fields['shipping']['shipping_state']);*/
		



		$fields['billing']['billing_first_name']['placeholder'] = 'First Name*';
		$fields['billing']['billing_first_name']['label_class'] = array("sr-only");

		$fields['billing']['billing_last_name']['placeholder'] = 'Last Name*';
		$fields['billing']['billing_last_name']['label_class'] = array("sr-only");

		$fields['billing']['billing_company']['placeholder'] = 'Company Name*';
		$fields['billing']['billing_company']['label_class'] = array("sr-only");
		$fields['billing']['billing_company']['class'] = array("d-none");

		$fields['billing']['billing_address_1']['placeholder'] = 'Street Address*';
		$fields['billing']['billing_address_1']['label_class'] = array("sr-only");

		$fields['billing']['billing_address_2']['placeholder'] = 'Unit/Apartment';
		$fields['billing']['billing_address_2']['label_class'] = array("sr-only");

		$fields['billing']['billing_city']['placeholder'] = 'City*';
		$fields['billing']['billing_city']['label_class'] = array("sr-only");

		$fields['billing']['billing_postcode']['placeholder'] = 'Postal Code*';
		$fields['billing']['billing_postcode']['label_class'] = array("sr-only");

		$fields['billing']['billing_country']['placeholder'] = 'Country*';
		$fields['billing']['billing_country']['label_class'] = array("sr-only");
		$fields['billing']['billing_country']['class'] = array("d-none");

		$fields['billing']['billing_state']['placeholder'] = 'Province*';
		$fields['billing']['billing_state']['label_class'] = array("sr-only");
		$fields['billing']['billing_state']['class'] = array("d-none");


		$fields['billing']['billing_email']['placeholder'] = 'Email*';
		$fields['billing']['billing_email']['label_class'] = array("sr-only");

		$fields['billing']['billing_phone']['placeholder'] = 'Phone';
		$fields['billing']['billing_phone']['label_class'] = array("sr-only");
		$fields['billing']['billing_phone']['required'] = false;
		



		$fields['delivery']['delivery_country'] = array(
            'type'          => 'country',
            'label'         => __('Country'),
            'required'        => true,
            'class'         => array('address-field form-row-wide update_totals_on_change d-none'),
            'autocomplete'    => "country",
            'priority'        => 40,
            'value' => "CA",
            'label_class'       => array("sr-only")
        );
        $fields['delivery']['delivery_address_1'] = array(
        		'label'         => __('Street Address'),
        		'placeholder'   => __('Street Address'),
        		'required'		=> true,
        		'class'         => array('address-field form-row-first half-row'),
        		'autocomplete'	=> "address-line1",
        		'priority'		=> 50,
        		'label_class'       => array("sr-only")
        );
        $fields['delivery']['delivery_address_2'] = array(
        	'placeholder'   => __('Unit/Apartment'),
        	'class'         => array('address-field form-row-last'),
        	'autocomplete'	=> "address-line2",
        	'priority'		=> 60,
        	'required'		=> false,
        	'label'         => __('Apartment, suite, unit etc. (optional)'),
        	'label_class'	=> "screen-reader-text",
        	'label_class'       => array("sr-only")
        ); 
        $fields['delivery']['delivery_city'] = array(
        	'placeholder'   => __('City*'),
        	'class'         => array('address-field form-row-first'),
        	'autocomplete'	=> "address-level2",
        	'priority'		=> 70,
        	'required'		=> true,
        	'label'         => __('Town / City'),
        	'label_class'       => array("sr-only")
   		);
		$fields['delivery']['delivery_state'] = array(
        	'type'          => 'state',
        	'label'         => __('Province'),
        	'required'		=> true,
        	'class'         => array('address-field form-row-wide d-none'),
        	'validate'		=> array('state'),
        	'autocomplete'	=> "address-level1",
        	'priority'		=> 90,
        	'country_field'	=> "delivery_country",
        	'country'		=> "CA",
        	'value'			=> "BC",
        	'label_class'       => array("sr-only")  			
  		);
		$fields['delivery']['delivery_postcode'] = array(
        	'label'         => __('Postcode / ZIP'),
        	'required'		=> true,
        	'class'         => array('address-field form-row-last'),
        	'validate'		=> array('postcode'),
        	'autocomplete'	=> "postal-code",
        	'priority'		=> 80,
        	'placeholder'   => __('Postal Code*'),
        	'label_class'       => array("sr-only")
        );      			         			

		$fields['delivery']['delivery_date'] = array(
			'label' => __('Delivery Date', 'woocommerce'),
			'placeholder'	=> _x('Delivery Date', 'placeholder', 'woocommerce'),
			'required'  	=> true,
			'class'     	=> array('form-row-first calendar-date delivery-date'),
			'clear'			=> true	,
			'label_class'   => array("sr-only"),
			'priority'		=> 100
		);
		$fields['delivery']['delivery_time'] = array(
			'label' => __('Delivery Time*', 'woocommerce'),
			'placeholder'	=> _x('Delivery Time', 'placeholder', 'woocommerce'),
			'required'  	=> true,
			'class'     	=> array('form-row-wide timepicker delivery-time'),
			'clear'			=> true,
			'type'			=> 'select',
			'options'		=> array(
				'select'	=> __('Delivery Time*'),
				'morning' 	=> __('8:00 - 12:00'),
				'afternoon' => __('12:00 - 4:00') 
			),
			'label_class'   => array("sr-only"),
			'priority'		=> 110
		);
		$fields['delivery']['delivery_notes'] = array(
			'label' => __('Delivery Notes', 'woocommerce'),
			'placeholder'	=> _x('Delivery Notes', 'placeholder', 'woocommerce'),
			'required'  	=> false,
			'class'     	=> array('form-row-wide delivery-notes long-field'),
			'clear'			=> true,
			'type'			=> 'textarea',
			'label_class'       => array("sr-only"),
			'priority'		=> 120	
		);
		$fields['pickup']['pickup_country'] = array(
            'type'          => 'country',
            'label'         => __('Country'),
            'required'        => true,
            'class'         => array('address-field form-row-wide update_totals_on_change d-none'),
            'autocomplete'    => "country",
            'priority'        => 40,
            'value' => "CA",
            'label_class'       => array("sr-only")

        );
        $fields['pickup']['pickup_address_1'] = array(
        		'label'         => __('Street Address'),
        		'placeholder'   => __('Street Address*'),
        		'required'		=> true,
        		'class'         => array('address-field form-row-wide'),
        		'autocomplete'	=> "address-line1",
        		'priority'		=> 50,
        		'label_class'       => array("sr-only")
        );
        $fields['pickup']['pickup_address_2'] = array(
        	'placeholder'   => __('Unit/Apartment'),
        	'class'         => array('address-field form-row-wide'),
        	'autocomplete'	=> "address-line2",
        	'priority'		=> 60,
        	'required'		=> false,
        	'label'         => __('Apartment, suite, unit etc. (optional)'),
        	'label_class'       => array("sr-only")

        ); 
        $fields['pickup']['pickup_city'] = array(
        	'placeholder'   => __('City*'),
        	'class'         => array('address-field form-row-wide'),
        	'autocomplete'	=> "address-level2",
        	'priority'		=> 70,
        	'required'		=> true,
        	'label'         => __('Town / City'),
        	'label_class'       => array("sr-only")
   		);
		$fields['pickup']['pickup_state'] = array(
        	'type'          => 'state',
        	'label'         => __('Province'),
        	'required'		=> true,
        	'class'         => array('address-field form-row-wide d-none'),
        	'validate'		=> array('state'),
        	'autocomplete'	=> "address-level1",
        	'priority'		=> 80,
        	'country_field'	=> "pickup_country",
        	'country'		=> "CA",  
        	'value'			=> "BC",
        	'label_class'       => array("sr-only")  			
  		);
		$fields['pickup']['pickup_postcode'] = array(
        	'label'         => __('Postcode / ZIP'),
        	'required'		=> true,
        	'class'         => array('address-field form-row-wide'),
        	'validate'		=> array('postcode'),
        	'autocomplete'	=> "postal-code",
        	'priority'		=> 90,
        	'label_class'       => array("sr-only"),
        	'placeholder'	=> "Postal Code*"
        );      			         			
		$fields['pickup']['pickup_date'] = array(
			'label' => __('Pickup Date', 'woocommerce'),
			'placeholder'	=> _x('Pickup Date*', 'placeholder', 'woocommerce'),
			'required'  	=> true,
			'class'     	=> array('form-row-wide calendar-date pickup-date'),
			'clear'			=> true	,
			'label_class'       => array("sr-only")
        );
		$fields['pickup']['pickup_time'] = array(
        	'label'         => __('Pickup Time'),
        	'placeholder'   => __('Pickup Time'),
        	'required'		=> true,
        	'class'         => array('form-row-wide timepicker delivery-time'),
        	'clear'			=> true,
			'type'			=> 'select',
			'options'		=> array(
				'select'	=> __('Pickup Time*'),
				'morning' 	=> __('8:00 - 12:00'),
				'afternoon' => __('12:00 - 4:00'),

			),
			'label_class'       => array("sr-only")	
        );
        $fields['pickup']['pickup_notes'] = array(
        	'label'         => __('Pickup Notes'),
        	'placeholder'   => __('Pickup Notes'),
        	'required'		=> false,
        	'class'         => array('form-row-wide pickup-notes long-field'),
        	'clear'			=> true,
        	'type'			=> "textarea",
        	'label_class'       => array("sr-only")
		);

		return $fields;
	}

	// Adds Custom Checkout Fields to Order Page
	add_action( 'woocommerce_checkout_update_order_meta', 'update_order_meta');
	function update_order_meta($order) {
		//lg_write_log($order);
		// TODO ADD IF WRAPPERS
			if (! empty($_POST['delivery_address_1'])) {
	        	update_post_meta( $order, 'delivery_address_1', sanitize_text_field( $_POST['delivery_address_1'] ) );
			}
			if (! empty($_POST['delivery_address_2'])) {
	        	update_post_meta( $order, 'delivery_address_2', sanitize_text_field( $_POST['delivery_address_2'] ) );
			}
			if (! empty($_POST['delivery_city'])) {
	        	update_post_meta( $order, 'delivery_city', sanitize_text_field( $_POST['delivery_city'] ) );
			}
			if (! empty($_POST['delivery_state'])) {
	        	update_post_meta( $order, 'delivery_state', sanitize_text_field( $_POST['delivery_state'] ) );
			}
			if (! empty($_POST['delivery_postcode'])) {
	        	update_post_meta( $order, 'delivery_postcode', sanitize_text_field( $_POST['delivery_postcode'] ) );
			}			
			if (! empty($_POST['delivery_date'])) {
        		update_post_meta( $order, 'delivery_date', sanitize_text_field( $_POST['delivery_date'] ) );
        	}
        	if (! empty($_POST['delivery_time'])) {
        		update_post_meta( $order, 'delivery_time', sanitize_text_field( $_POST['delivery_time'] ) );
        	}
        	if (! empty($_POST['delivery_notes'])) {
        		update_post_meta( $order, 'delivery_notes', sanitize_text_field( $_POST['delivery_notes'] ) );
        	}
        	if (! empty($_POST['pickup_address_1'])) {
	        	update_post_meta( $order, 'pickup_address_1', sanitize_text_field( $_POST['pickup_address_1'] ) );
			}
			if (! empty($_POST['pickup_address_2'])) {
	        	update_post_meta( $order, 'pickup_address_2', sanitize_text_field( $_POST['pickup_address_2'] ) );
			}
			if (! empty($_POST['pickup_city'])) {
	        	update_post_meta( $order, 'pickup_city', sanitize_text_field( $_POST['pickup_city'] ) );
			}
			if (! empty($_POST['pickup_state'])) {
	        	update_post_meta( $order, 'pickup_state', sanitize_text_field( $_POST['pickup_state'] ) );
			}
			if (! empty($_POST['pickup_postcode'])) {
	        	update_post_meta( $order, 'pickup_postcode', sanitize_text_field( $_POST['pickup_postcode'] ) );
			}
			if (! empty($_POST['pickup_date'])) {
	        	update_post_meta( $order, 'pickup_date', sanitize_text_field( $_POST['pickup_date'] ) );
			}
			if (! empty($_POST['pickup_time'])) {
	        	update_post_meta( $order, 'pickup_time', sanitize_text_field( $_POST['pickup_time'] ) );
			}
			if (! empty($_POST['pickup_notes'])) {
	        	update_post_meta( $order, 'pickup_notes', sanitize_text_field( $_POST['pickup_notes'] ) );
			}	
	}


	// Add Fields to Order Page in Shipping Section
	add_action( 'woocommerce_admin_order_data_after_shipping_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
	function my_custom_checkout_field_display_admin_order_meta($order){
/*		echo '<p><strong>'.__('Delivery Address 1').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_address_1', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Address 2').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_address_2', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery City').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_city', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Province').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_state', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Postcode').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_postcode', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Date').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_date', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Time').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_time', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Notes').':</strong> ' . get_post_meta( $order->get_id(), 'delivery_notes', true ) . '</p>';*/
	}

	add_action('add_meta_boxes', 'lg_add_meta_boxes');
	function lg_add_meta_boxes() {
        add_meta_box( 'pickup_fields', __('Delivery & Pickup Details','woocommerce'), 'pickup_meta_box', 'shop_order', 'normal','high');
	}

	function pickup_meta_box($order) {
		//var_dump($order->ID);
		//echo "TEST";
		echo '<div class="d-flex justify-content-left panel" style="padding: 23px 24px 12px;">';

		echo '<div class="delivery-details" style="padding-right:20%">';
		echo '<p class="h5 text-primary"><strong>Delivery Address</strong></p>';
		echo '<p><strong>'.__('Delivery Address 1').':</strong> ' . get_post_meta( $order->ID, 'delivery_address_1', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Address 2').':</strong> ' . get_post_meta( $order->ID, 'delivery_address_2', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery City').':</strong> ' . get_post_meta( $order->ID, 'delivery_city', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Province').':</strong> ' . get_post_meta( $order->ID, 'delivery_state', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Postcode').':</strong> ' . get_post_meta( $order->ID, 'delivery_postcode', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Date').':</strong> ' . get_post_meta( $order->ID, 'delivery_date', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Time').':</strong> ' . get_post_meta( $order->ID, 'delivery_time', true ) . '</p>';
    	echo '<p><strong>'.__('Delivery Notes').':</strong> ' . get_post_meta( $order->ID, 'delivery_notes', true ) . '</p>';
    	echo '</div>';


		echo '<div class="pickup-details">';
		echo '<p class="h5 text-primary"><strong>Pickup Address</strong></p>';
    	echo '<p><strong>'.__('Pickup Address 1').':</strong> ' . get_post_meta( $order->ID, 'pickup_address_1', true ) . '</p>';
    	echo '<p><strong>'.__('Pickup Address 2').':</strong> ' . get_post_meta( $order->ID, 'pickup_address_2', true ) . '</p>';
    	echo '<p><strong>'.__('Pickup City').':</strong> ' . get_post_meta( $order->ID, 'pickup_city', true ) . '</p>';
    	echo '<p><strong>'.__('Pickup Province').':</strong> ' . get_post_meta( $order->ID, 'pickup_state', true ) . '</p>';
    	echo '<p><strong>'.__('Pickup Postcode').':</strong> ' . get_post_meta( $order->ID, 'pickup_postcode', true ) . '</p>';
    	echo '<p><strong>'.__('Pickup Date').':</strong> ' . get_post_meta( $order->ID, 'pickup_date', true ) . '</p>';
    	echo '<p><strong>'.__('Pickup Time').':</strong> ' . get_post_meta( $order->ID, 'pickup_time', true ) . '</p>';
    	echo '<p><strong>'.__('Pickup Notes').':</strong> ' . get_post_meta( $order->ID, 'pickup_notes', true ) . '</p>';
    	echo '</div>';

    	echo '</div>';

	}

	add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );
	function change_default_checkout_country() {
  		return 'CA'; // country code
	}

	add_filter( 'default_checkout_billing_state', 'change_default_checkout_state' );
	function change_default_checkout_state() {
		return 'BC'; // state code
	}

	add_filter( 'woocommerce_states', "lg_restrict_states");
	function lg_restrict_states($states) {
		$states['CA'] = array(
		'BC' => __( 'British Columbia', 'woocommerce' )

		);

		return $states;
	}

	add_filter( 'woocommerce_countries', 'lg_restrict_countries');
	function lg_restrict_countries($countries) {
		$countries = array(
			'CA' => __( 'Canada', 'woocommerce' ),
		);

		return $countries;
	}

	add_action('woocommerce_before_calculate_totals', 'update_cart_price');
	function update_cart_price( $cart_object ) {  
	    foreach($cart_object->get_cart() as $cart_item_key => $cart_item) {

	        if (has_term(array('boxes', 'residential', 'commercial', 'packages'), 'product_cat', $cart_item['product_id'])) {

	        	$price = $cart_item['rental_price'];
	        	//lg_write_log($price);

	            $cart_item['data']->set_price($price);
	            $cart_item['data']->set_regular_price($price);

	            //lg_write_log($cart_item['data']->get_price());

	        }  
	    }  
	}

	add_filter('woocommerce_form_field', 'checkout_fields_in_label_error', 10, 4);
	function checkout_fields_in_label_error($field, $key, $args, $value) {
		if (strpos($field, '</label>') !== false && $args['required'] ) {
	      $error = '<span class="error" style="display:none">';
	      $error .= sprintf( __( '%s is required.', 'woocommerce' ), $args['label'] );
	      $error .= '</span>';
	      //$field = substr_replace( $field, $error, strpos( $field, '</label>' ), 0);	 
	      $field = substr_replace( $field, $error, strpos( $field, '</p>'), 0);     
		}
		return $field;
	}

	add_action('template_redirect', 'lg_order_confirm');

	function lg_order_confirm( ) {

		global $wp;

		if (is_checkout() &&  !empty( $wp->query_vars['order-received'] ) ) {
			lg_write_log($wp->query_vars['order-received']);
			wp_redirect ( get_site_url() . '/checkout/order-received/' );
			exit;
		}
	}

	add_filter( 'woocommerce_return_to_shop_redirect', 'lg_return_to_shop' );

	function lg_return_to_shop() {
		return get_site_url() . "/order";
	}


	add_action( 'lg_rentals_email_customer_delivery_pickup', 'lg_rental_details', 20, 4);

	function lg_rental_details($order, $sent_to_admin = false, $plain_text = false ) {
		if(! is_a($order, 'WC_Order')) {
			return;

		}

		$fields = array (
			"Delivery" => array (
				"Address" => get_post_meta( $order->get_id(), 'delivery_address_1', true ),
				"Address 2" => get_post_meta( $order->get_id(), 'delivery_address_2', true ),
				"City" => get_post_meta( $order->get_id(), 'delivery_city', true ),
				"Province" => get_post_meta( $order->get_id(), 'delivery_state', true ),
				"Postal Code" => get_post_meta( $order->get_id(), 'delivery_postcode', true ),
				"Date" => get_post_meta( $order->get_id(), 'delivery_date', true ),
				"Time" => get_post_meta( $order->get_id(), 'delivery_time', true ),
				"Notes" => get_post_meta( $order->get_id(), 'delivery_notes', true )
			),
			"Pickup" => array (
				"Address" => get_post_meta( $order->get_id(), 'pickup_address_1', true ),
				"Address " => get_post_meta( $order->get_id(), 'pickup_address_2', true ),
				"City" => get_post_meta( $order->get_id(), 'pickup_city', true ),
				"Province" => get_post_meta( $order->get_id(), 'pickup_state', true ),
				"Postal Code" => get_post_meta( $order->get_id(), 'pickup_postcode', true ),
				"Date" => get_post_meta( $order->get_id(), 'pickup_date', true ),
				"Time" => get_post_meta( $order->get_id(), 'pickup_time', true ),
				"Notes" => get_post_meta( $order->get_id(), 'pickup_notes', true )
			)
		);

		if ( ! empty( $fields ) ) {
			if ( $plain_text ) {
				wc_get_template( 'emails/plain/email-customer-rental-details.php', array( 'fields' => $fields ) );
			} else {
				wc_get_template( 'emails/email-customer-rental-details.php', array( 'fields' => $fields, 'order' => $order ) );
			}
		}
	}




/*	add_filter('woocommerce_checkout_update_order_review_expired', function() {
		return 0;
	});*/
	//lg_write_log(apply_filters( 'woocommerce_checkout_update_order_review_expired', true )) 


	/*add_filter('woocommerce_email_order_meta_fields', 'lg_woo_email_meta_fields');
	function lg_woo_email_meta_fields( $fields, $sent_to_admin, $order) {

		var_dump($fields);
	}*/


?>