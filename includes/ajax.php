<?php

	if ( ! function_exists('lg_write_log')) {
    	function lg_write_log ( $log )  {
        	if ( is_array( $log ) || is_object( $log ) ) {
           		error_log( print_r( $log, true ) );
        	} else {
           		error_log( $log );
        	}
    	}
  	}

	// Ajax Call to Redraw Cart via Ajax
	add_action('wp_ajax_nopriv_replaceCart', 'replaceCart');
	add_action('wp_ajax_replaceCart', 'replaceCart');

	function replaceCart() {
		echo do_shortcode("[woocommerce_cart]");
		wp_die();
	}

	// Ajax Call to Redraw Checkout via Ajax
	add_action('wp_ajax_nopriv_replaceCheckout', 'replaceCheckout');
	add_action('wp_ajax_replaceCheckout', 'replaceCheckout');

	function replaceCheckout() {
		ob_start(); ?>

        <?php wc_get_template('checkout/review-order.php', array('checkout' => WC()->checkout(),)); ?>
			
		<?php echo ob_get_clean();
        //WC_AJAX::update_order_review();
		wp_die();
	}

	// Ajax Call to Add Item to Cart
	add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
	add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
        
	function woocommerce_ajax_add_to_cart() {
            $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
            $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
            $variation_id = absint($_POST['variation_id']);
            $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
            $product_status = get_post_status($product_id);

            if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

                do_action('woocommerce_ajax_added_to_cart', $product_id);

                if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                    wc_add_to_cart_message(array($product_id => $quantity), true);
                }

                WC_AJAX :: get_refreshed_fragments();
            } else {

                $data = array(
                    'error' => true,
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

                echo wp_send_json($data);
            }

            wp_die();
        }

    // Adjax Call to Remove Item from Cart
    add_action('wp_ajax_remove_item_from_cart', 'remove_item_from_cart');
    add_action('wp_ajax_nopriv_remove_item_from_cart', 'remove_item_from_cart');


    function remove_item_from_cart() {
	    $cart = WC()->instance()->cart;
	    $id = $_POST['product_id'];
	    $cart_id = $cart->generate_cart_id($id);
	    $cart_item_id = $cart->find_product_in_cart($cart_id);

	    foreach($cart->get_cart() as $cart_item_key => $cart_item) {
	    	if($cart_item['product_id'] == $id) {
	    		//lg_write_log($cart_item);
	    		$cart->set_quantity($cart_item['key'], 0);
	    	}
	   	}
	    
	    }


	add_action('wp_ajax_lg_calculate_price', 'lg_calculate_price');
	add_action('wp_ajax_nopriv_lg_calculate_price', 'lg_calculate_price');

	function lg_calculate_price() {
		$product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
		$price = absint($_POST['price']);
		$quantity = 1;
		$passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
		$product_status = get_post_status($product_id);
        $rental_price = array ('rental_price' => $price);
        //lg_write_log($rental_price);
        $already_in_cart = false;

        $cart_obj = WC()->instance()->cart;
        foreach($cart_obj->get_cart() as $cart_item_key => $cart_item) {
            if (has_term('boxes', 'product_cat', $cart_item['product_id'])) {
                    $already_in_cart = true;
                }
        }

        if ($already_in_cart == false) {
		    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variation, $rental_price) && 'publish' === $product_status) {
            
                do_action('woocommerce_ajax_added_to_cart', $product_id);



                if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                    wc_add_to_cart_message(array($product_id => $quantity), true);
                }
 
                WC_AJAX :: get_refreshed_fragments();
		    }
		    else {

                $data = array(
                    'error' => true,
                    'passed_validation' => $passed_validation,
                    'publish'       => $product_status,
                    'add_to_cart'   => WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variation, $rental_price),
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'variation_id' => $variation,
                    'rental_price' => $rental_price,
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id)
                );
                echo wp_send_json($data);
            }
        }
    }

    add_action('wp_ajax_box_product_slider', 'box_product_slider');
    add_action('wp_ajax_nopriv_box_product_slider', 'box_product_slider');

    function box_product_slider() {

        $business_type = $_POST['businessType'];
        $package_type = $_POST['packageType'];

        lg_write_log($business_type);
        lg_write_log($package_type);

        if($business_type == 'residential' && $package_type == 'boxes' || $business_type == 'commercial' && $package_type == 'boxes') {
                $post_cat = 'boxes';
        }
        elseif($business_type == 'residential' && $package_type == 'packages') {
            $post_cat = 'residential';
        }
        elseif($business_type == 'commercial' && $package_type == 'packages') {
            $post_cat = 'commercial';
        }
        else {
            $post_cat = 'error';
        }

        lg_write_log($post_cat);





        $args = array(
            'post_type' => 'product',
            'posts_per_page' => '-1',
            'order' =>  'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    'terms' => $post_cat
                )   
            )
        );

        $product_query = new WP_Query ($args);

        //ob_start() ?>

        <?php  if ($product_query->have_posts() ): ?>
            <?php while ($product_query->have_posts() ) : $product_query->the_post(); ?>
                <?php 
                    global $product;
                    global $post;    
                    $product_image = get_the_post_thumbnail_url();
                    $product_slug = $product->get_slug();
                    $product_id = $product->get_id();

                    $product_base_price = get_post_meta($post->ID, 'rental_base_price', true);
                    $product_block_price = get_post_meta($post->ID, 'rental_block_price', true); 

                            
                    ?>

                    <div class="product-slide slide d-flex flex-column align-items-center justify-content-center " id="<?php echo $product_slug; ?>" data-product-base-price="<?php echo $product_base_price; ?>" data-product-block-price="<?php echo $product_block_price; ?>" data-product-id="<?php echo $product_id ?>">   
                        <?php echo file_get_contents($product_image); ?>
                        <span class="product-name font-weight-bold"><?php echo get_the_title(); ?></span>
                        <div class="product-description d-none"><?php echo $product->get_description(); ?></div>
                        <?php lg_write_log($product_slug) ?>
                    </div>
            <?php   endwhile; ?>
            <?php lg_write_log("---------------------") ?>
            <?php   else:
                echo "No Posts Found";
            ?>
        <?php   endif; ?>  

        <?php wp_reset_postdata() ?>

        <?php 

        //echo ob_get_clean();

        wp_die();
        
    }

    add_action('wp_ajax_lg_update_cart_quantity', 'lg_update_cart_quantity');
    add_action('wp_ajax_nopriv_lg_update_cart_quantity', 'lg_update_cart_quantity');

    function lg_update_cart_quantity() {
        $quantity = $_POST['quantity'];
        $cart_item = $_POST['cart_item'];
        $max_quantity = $_POST['max_quantity'];

        global $woocommerce;


        if ($max_quantity != "none") {
            if ( $quantity >= $max_quantity) {
                $quantity = $max_quantity;
            }
        }

        $woocommerce->cart->set_quantity($cart_item, $quantity);

        WC_AJAX :: get_refreshed_fragments();

        wp_die();
    }
?>