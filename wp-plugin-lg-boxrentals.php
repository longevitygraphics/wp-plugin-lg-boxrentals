<?php
/**
* Plugin Name: LG Box Rentals
* Plugin URI: https://www.longevitygraphics.com/
* Author: Stefan Adam,
* Version: 0.1a
*/

defined( 'ABSPATH' ) or die();

  if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }

 function lg_boxrental_init() {
 	require_once 'includes/ajax.php';
	require_once 'includes/woocommerce.php';
 }



/**********************
Assets 
************************/

function lg_boxrental_plugin_scripts(){
	lg_write_log(get_option( 'woocommerce_checkout_page_id' ));
	if (is_page(get_option( 'woocommerce_checkout_page_id' ))) {
		wp_register_script( 'lg-boxrental-script', plugins_url( '/assets/dist/js/main.min.js', __FILE__ ), array('jquery') );
		wp_enqueue_script( 'lg-boxrental-script' );
	}
	

/*	wp_register_style( 'lg-plugin-style', plugins_url( '/assets/lg-style.min.css', __FILE__ ) );
	wp_enqueue_style( 'lg-plugin-style' );*/
}

add_action( 'wp_enqueue_scripts', 'lg_boxrental_plugin_scripts' );





/**********************
Dependencies checking 
************************/
function lg_boxrentals_admin_notice__error($message) {
	$class = 'notice notice-error';

	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}


function lg__box_rental_plugin_dependencies_check() {
    if (!function_exists('get_plugins')) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $plugins = get_plugins();
    $error_message = '';

    //lg_write_log($plugins);

    if (isset($plugins['woocommerce/woocommerce.php'])) {
    	
		if(!is_plugin_active('woocommerce/woocommerce.php')){
			$error_message .= 'LG Box Rentals : Woocommerce is not Activated .';
		}else if($plugins['woocommerce/woocommerce.php']['Version'] < 3.6){
			$error_message .= 'LG Box Rentals : Woocommerce 3.6.4 or Greater Required';
		}
    }else{
    	$error_message .= 'LG Box Rentals : Woocommerce is not Detected';
    }

	if($error_message == ''){
		add_action( 'plugins_loaded', 'lg_boxrental_init' );
	}else{
		add_action( 'admin_notices', function() use ($error_message) {
			$class = 'notice notice-error';
			lg_boxrentals_admin_notice__error($error_message);
		}
	 );
	}
}

lg__box_rental_plugin_dependencies_check();

?>